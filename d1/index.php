<?php

	//$_GET and $_POST are "super global" variables in PHP. 

	//Always accessible regardless the scope
	//"super global" variable allows data to persist between pages or a single session.

	//"values" represent the input data from the user. 
	// 'isset()' function checks whether a variable is set.
	//true if variable is set, false if the return is null

$tasks= ["Get git", "Bake HTML", "Eat CSS", "Learn PHP"];
	
	if(isset($_GET['index'])){
/*	echo $_GET['index'];
	var_dump($_GET);*/

	$indexGet= $_GET["index"];
	echo "The retrieved task from GET is $tasks[$indexGet]<br>";
	};

	if(isset($_POST['index'])){
/*	echo $_POST['index'];
	var_dump($_POST);*/

	$indexPost= $_POST["index"];
	echo "The retrieved task from POST is $tasks[$indexPost]<br>";
	}

	
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05: Client-Server Communication (GET AND POST)</title>
</head>
<body>
	<h1>Task Index from GET</h1>

	<form method="GET">

		<!-- mail: <input type="text" name="email"> <br>
		Password: <input type="password" name="password"> <br> -->
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>
	</form>

	<h1>Task Index from POST</h1>

		<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>

</body>
</html>